#!/usr/bin/env python

import os

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

# Read the contents of your README file
this_directory = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(this_directory, 'README.md')) as f:
    readme = f.read()

setup(name='gridengine-goodies',
      version='1.2.1',
      description='Enhancements to the gridengine scheduler',
      long_description=readme,
      long_description_content_type='text/markdown',
      author='Daniele Coslovich',
      author_email='daniele.coslovich@umontpellier.fr',
      url='https://gitlab.info-ufr.univ-montp2.fr/daniele.coslovich/gridengine-goodies',
      packages=[],
      license='GPLv3',
      install_requires=[],
      scripts=['bin/qbarrier', 'bin/qdelx', 'bin/qexec', 'bin/qgrep', 'bin/qstatx', 'bin/qsubx'],
      classifiers=[
          'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
          'Development Status :: 5 - Production/Stable',
          'Programming Language :: Python :: 2',
          'Programming Language :: Python :: 2.7',
          'Programming Language :: Python :: 3',
          'Programming Language :: Python :: 3.4',
          'Topic :: System',
      ]
     )
